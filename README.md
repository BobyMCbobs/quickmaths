# quickmaths
This is a simple Electron based maths app which I wrote for something, but isn't going to be used anymore.  
Here's the project - note, it's <b>not the best thing</b> that I've written (whatsoever).  
I initally wrote it when I was beginning JavaScript, so the code wasn't so great... I've cleaned it up a bit for this release.  

# Setup
`npm i`  

# Run
`npm start`  

## License
Copyright 2019 BobyMCbobs.  
This project is licensed under the [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).  
This program comes with absolutely no warranty.  
