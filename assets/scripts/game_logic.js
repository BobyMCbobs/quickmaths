// quickmaths

//
// Copyright (C) 2018 Caleb Woodbine <calebwoodbine.public@gmail.com>
//
// This file is part of quickmaths.
//
// quickmaths is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// quickmaths is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with quickmaths.  If not, see <https://www.gnu.org/licenses/>.
//

const $ = require("jquery");

const mode_list = [" + ", " - ", " x ", " ÷ "];
var operand,
  wildcard,
  game_lock,
  round_question,
  first_num,
  second_num,
  score_box;

function clear_welcome() {
  $(".greeting_scene").hide();
}

function go_to_game() {
  $(".game_scene").show();
  $(".game_type_select_scene").hide();
  $('.credits_msg').text(`Quick Maths`);
  init_game();
  begin_round();
}

function game_type_select() {
  $(".game_type_select_scene").show();
  wildcard=false;
}

function select_mode(mode) {
  var game_mode = mode;

  // Addition
  if (game_mode == 0) operand = mode_list[0];
  // Subtraction
  else if (game_mode == 1) operand = mode_list[1];
  // Multiplication
  else if (game_mode == 2) operand = mode_list[2];
  // Division
  else if (game_mode == 3) operand = mode_list[3];
  // Wildcard
  else if (game_mode == 10) wildcard = true;
}

function return_welcome() {
  $(".greeting_scene").show();
  $(".game_scene").hide();
  $(".game_type_select_scene").hide();
  $('.credits_msg').text('Quick Maths (v1); Licensed under GPL3.0.');
  game_lock = true;
}

function correct_answer() {
  $('#game_question').text("Well done!");
  game_lock = true;

  setTimeout(begin_round, 2000);
}

function incorrect_answer(real_answer) {
  $('#game_question').text(`Correct answer: ${real_answer}`);
  game_lock = true;

  setTimeout(begin_round, 2500);
}

function use_chance() {
  $('#game_question').text("Sorry, try again.");
  game_lock = true;

  setTimeout(function() {
    $('#game_question').text(`What's ${first_num} ${operand} ${second_num}?`);
    game_lock = false;
  }, 1000);
}

function init_game() {
  score_box = [0, 0, 0, 0];
  game_lock = false;
}

function check_answer() {
  var answer = $('#answer_to_problem').val(),
    real_answer;

  if (operand == " + ") real_answer = first_num + second_num;
  else if (operand == " - ") real_answer = first_num - second_num;
  else if (operand == " x ") real_answer = first_num * second_num;
  else if (operand == " ÷ ") real_answer = first_num / second_num;
  else if (operand == "words") real_answer = make_number_to_word(number_to_be_letters);
  real_answer = String(real_answer);

  if (answer === real_answer) {
    //console.log('Correct.');
    score_box[0] += 1;
    update_scores();
    correct_answer();
  }

  else if (answer != real_answer && score_box[3] == 2) {
    //console.log('Wrong.');
    score_box[1] += 1;
    incorrect_answer(real_answer);
  }

  else {
    score_box[3] += 1;
    use_chance()
  }
  update_scores();
}

function game_button() {
  if (game_lock === false) {
    check_answer();
  }
}

function reg_enter() {
  game_button();
}

function update_scores() {
  $('#user_points').text(`${score_box[0]} correct, ${score_box[1]} incorrect, ${score_box[3]}/2 chances."`);
}

function make_number_to_word(num) {
  return numberToWords.toWords(num);
}

function begin_round() {
  $('#answer_to_problem').val("");
  score_box[3] = 0;
  update_scores()
  game_lock = false;
  if (wildcard == true) {
    operand = mode_list[Math.floor((Math.random() * 3) + 0)];
  }

  if (operand == " + ") {
    first_num = Math.floor((Math.random() * 100) + 1);
    second_num = Math.floor((Math.random() * 100) + 1);
  }

  else if (operand == " - ") {
    first_num = Math.floor((Math.random() * 100) + 1);
    second_num = Math.floor((Math.random() * 100) + 1);
  }

  else if (operand == " ÷ ") {
    first_num = Math.floor((Math.random() * 100) + 1);
    second_num = Math.floor((Math.random() * 12) + 1);
    while (isInt(first_num/second_num) == false) {
      first_num = Math.floor((Math.random() * 100) + 1);
      second_num = Math.floor((Math.random() * 12) + 1);
    }
  }

  else if (operand == " x ") {
    first_num = Math.floor((Math.random() * 12) + 1);
    second_num = Math.floor((Math.random() * 12) + 1);
  }

  console.log({operand, first_num, second_num});

  $("#answer_to_problem").focus();
  $('#game_question').text(`What's ${first_num} ${operand} ${second_num}?`);
}

function isInt(n){
    return Number(n) === n && n % 1 === 0;
}

$('#start_button').bind('click', () => {
  clear_welcome();
  game_type_select();
});

$('#mode_select_buttons_addition').bind('click', () => {
  select_mode(0);
  go_to_game();
});

$('#mode_select_buttons_subtraction').bind('click', () => {
  select_mode(1);
  go_to_game();
});

$('#mode_select_buttons_multiplication').bind('click', () => {
  select_mode(2);
  go_to_game();
});

$('#mode_select_buttons_division').bind('click', () => {
  select_mode(3);
  go_to_game();
});

$('#mode_select_buttons_wildcard').bind('click', () => {
  select_mode(10);
  go_to_game();
});

$('#answer_button').bind('click', game_button);
$('#close_button').bind('click', return_welcome);

return_welcome();
