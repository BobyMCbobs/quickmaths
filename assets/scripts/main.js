// quickmaths

//
// Copyright (C) 2018 Caleb Woodbine <calebwoodbine.public@gmail.com>
//
// This file is part of quickmaths.
//
// quickmaths is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// quickmaths is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with quickmaths.  If not, see <https://www.gnu.org/licenses/>.
//

const electron = require('electron'),
  app = electron.app,
  BrowserWindow = electron.BrowserWindow,
  path = require('path'),
  url = require('url');

let mainWindow;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
	  width: 1000,
	  height: 700,
	  minWidth: 550,
   	minHeight: 650,
	  icon: path.join(__dirname, '..', 'icons', 'png', 'quick-maths-logo_512x512.png')
	});

  //mainWindow.setMenu(null);

  // load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, '..', 'html', 'index.html'));
  mainWindow.on('closed', function () {
    mainWindow = null
  });
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});
